+++
title = "TheAlternative (voluntary work)"
weight = 4
[extra]
project_link = "https://www.thealternative.ch/"
programming_technologies = "Linux, Bash, Latex"
+++

<div class="fancyPositioning">
{{picture(path="console_toolkit.png", caption="console toolkit course", link="https://gitlab.ethz.ch/thealternative/courses/-/blob/71722d82f977e12a23b27f0b902f185e4768c847/console_toolkit/console_toolkit_1.pdf")}}
{{picture(path="console_toolkit_exercises.png", caption="console toolkit exercise sheet", link="https://gitlab.ethz.ch/thealternative/courses/-/blob/71722d82f977e12a23b27f0b902f185e4768c847/console_toolkit/exercise_files/exercises.pdf")}}
</div>

<p>
{{linkAndLogo(text="TheAlternative", icon="thealt.png", link="https://www.thealternative.ch/")}} is a small student group (5-15 members) and part of the larger {{linkAndLogo(link="https://vseth.ethz.ch/en/", icon="ethz.png", text="VSETH")}} student association. Our focus lies on promoting Linux and Free and Open Source Software. We teach introductory Linux courses for students and help them install Linux at a dedicated "install event" once per semester. The organization lives entirely from voluntary work.
</p>

I typically teach one course per semester - usually the [Console Toolkit](https://gitlab.ethz.ch/thealternative/courses/-/blob/71722d82f977e12a23b27f0b902f185e4768c847/console_toolkit/console_toolkit_1.pdf) course - and have contributed to most of the guides on our publicly available [Gitlab repository](https://gitlab.ethz.ch/thealternative/courses/). I'm currently (2019-...) in the executive board and responsible for organizing the courses.

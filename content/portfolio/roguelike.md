+++
title = "Unnamed Roguelike (personal project)"
weight = 3
[extra]
project_link = "https://gitlab.com/PawkyPenguin/roguelikegamethingy/"
programming_technologies = "Java"
+++

<div class="fancyPositioning">
{{picture(path="roguelike.png", caption="First floor of the dungeon")}}
</div>

An unnamed dungeon-crawler game I worked on during my first year at ETH. Features a custom maze generator for the dungeon and a primitive crafting system. It's based on Java Swing, though I didn't really use any rendering primitives. The game draws everything into a large JFrame and animations are implemented by hand.

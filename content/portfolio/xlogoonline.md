+++
title = "XLogoOnline (bachelor thesis)"
weight = 0
[extra]
project_link = "https://xlogo.inf.ethz.ch/"
programming_technologies = "TypeScript, Angular, Ruby (on Rails)"
+++

<div class="fancyPositioning">
{{picture(path="xlogoonline.png", caption="main programming interface")}}
{{picture(path="bachelor_thesis.png", caption="bachelor thesis report", link="bachelor_thesis.pdf")}}
</div>


During my bachelor's thesis I worked on XLogoOnline, an IDE designed to teach children interactive programming with turtle graphics. The IDE uses a dialect of
{{linkAndLogo(text="Logo", icon="wikipedia.ico", link="https://en.wikipedia.org/wiki/Logo_%28programming_language%29")}}, called XLogo, that was developed at ETH. My thesis work consisted of implementing a cooperative mode that lets multiple turtles (children) work together in creating drawings. XLogoOnline is currently in use by [ABZ [german website]](https://www.abz.inf.ethz.ch/) in primary school education.

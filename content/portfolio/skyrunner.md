+++
title = "Sky Runner (student project)"
weight = 1
[extra]
programming_technologies = "C# (MonoGame), OpenGL"
+++

<div class="fancyPositioning">
{{video(id="P8wff2fd55M", caption="game trailer")}}
{{picture(path="skyrunner_startmenu_render.png", caption="start menu")}}
{{picture(path="skyrunner_level.png", caption="one of the playable levels")}}
</div>

Sky Runner was developed as a semester-long course project in the course 
{{linkAndLogo(text="GameLab", icon="ethz.png", link="https://gtc.inf.ethz.ch/education/game-programming-laboratory.html")}}
 in a group of four (Alain Senn, Livio Steiner, Yufeng Zheng and me). Sky Runner is a fast-paced four player party game where each player tries to complete their own rocket and tries to disturb the other players by pushing them off the platform.

We didn't have strict task divisions but I was mostly responsible for architectural decisions (together with Alain Senn) and writing OpenGL shaders. Using MonoGame was a course requirement, so we rolled our own entity component system that we attached to a 3rd party physics library. The game is currently not available for download.

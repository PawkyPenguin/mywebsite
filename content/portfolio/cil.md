+++
title = "Road Segmentation (student project)"
weight = 2
[extra]
project_link = "https://gitlab.ethz.ch/ablera/cil-roads"
programming_technologies = "Python, Tensorflow+Keras"
+++

<div class="fancyPositioning">
{{picture(path="cil_road.png", caption="aerial road image (input)")}}
{{picture(path="cil_road_masked.png", caption="segmented road image (output)")}}
{{picture(path="cil_report.png", caption="project report", link="cil_report.pdf")}}
</div>

This is another semester-long group project that Alain Senn, Aline Abler, Livio Steiner and I developed as part of the
{{linkAndLogo(text="CIL", icon="ethz.png", link="http://www.da.inf.ethz.ch/teaching/2019/CIL/")}}
 course. Using the [Keras](https://keras.io/) framework, we developed a custom neural network based on the [SegNet](https://arxiv.org/pdf/1511.00561) and [RedNet](https://arxiv.org/abs/1806.01054) architectures to classify roads of aerial view images. The network takes an aerial view image as an input and outputs a binary decision mask (road/no road).
